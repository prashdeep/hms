create database if not exists hms;
use hms;

-- Create table syntax starts --

CREATE TABLE `doctor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `specialization` enum('ORTHO','PADIETRIC','DENTAL','GYNAC','KNEE_SURGEON','HIP_SURGEON','PULMOLOGIST') DEFAULT NULL,
  `exp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


CREATE TABLE `patient` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `dob` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `appointment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `appointment_date` date NOT NULL,
  `consultation_fee` double NOT NULL,
  `doctor_id` int(10) unsigned DEFAULT NULL,
  `patient_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `doctor_id` (`doctor_id`),
  CONSTRAINT `appointment_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`id`),
  CONSTRAINT `appointment_ibfk_2` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


CREATE TABLE `prescription` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `appointment_id` int(10) unsigned DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `apointment_id` (`appointment_id`),
  CONSTRAINT `prescription_ibfk_1` FOREIGN KEY (`appointment_id`) REFERENCES `appointment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Create table syntax ends --

-- Sample data starts --

insert into doctor(name,specialization, exp) values
    ("Girish", "ORTHO", 6),
  	("Vinayaka", "ORTHO", 8),
	  ("Vinnet", "PADIETRIC", 2);

insert into patient (name, dob) values
	("Kiran", "2018-05-05"),
    ("Vikram", "2018-12-08"),
    ("Vinay", "2015-10-10"),
    ("Jeevan", "2012-08-08");

