package com.edureka.hms.exception;

/**
 * Created by prade on 4/27/2018.
 */
public class PatientNotFoundException extends Exception {

    public PatientNotFoundException(String message){
        super(message);
    }

    @Override
    public String getMessage(){
        return super.getMessage();
    }
}
