package com.edureka.hms.service;

import com.edureka.hms.dao.DoctorDAO;
import com.edureka.hms.dao.DoctorDAOImpl;
import com.edureka.hms.model.Appointment;
import com.edureka.hms.model.Doctor;

import java.util.List;

/**
 * Created by prade on 5/22/2018.
 */
public class DoctorServiceImpl implements DoctorService {
    private final DoctorDAO doctorDAO;

    public DoctorServiceImpl(DoctorDAO doctorDAO) {
        this.doctorDAO = doctorDAO;
    }

    @Override
    public List<Doctor> listAll() {
        return this.doctorDAO.listAll();
    }

    @Override
    public Doctor fetch(int doctorId) {
        return this.doctorDAO.fetch(doctorId);
    }

}
