package com.edureka.hms.service;

import com.edureka.hms.dao.AppointmentDAO;
import com.edureka.hms.dao.AppointmentDAOImpl;
import com.edureka.hms.model.Appointment;

import java.util.List;

/**
 * Created by prade on 4/27/2018.
 */
public class AppointmentServiceImpl implements AppointmentService {

    private AppointmentDAO appointmentDAO = new AppointmentDAOImpl();

    public AppointmentServiceImpl(AppointmentDAO appointmentDAO){
        this.appointmentDAO = appointmentDAO;
    }


    public void bookAppointment(Appointment appointment){
       this.appointmentDAO.bookAppointment(appointment);
    }

    @Override
    public List<Appointment> listAppointmentsByDoctor(int doctorId) {
        return appointmentDAO.listAppointmentsByDoctorId(doctorId);
    }

    @Override
    public int prescribeMedicine(int appointmentId, String prescription) {
        return appointmentDAO.prescribeMedicine(appointmentId, prescription);
    }
}
