package com.edureka.hms.service;

import com.edureka.hms.dao.PatientDAOImpl;
import com.edureka.hms.model.Patient;

import java.util.List;

/**
 * Created by prade on 5/22/2018.
 */
public class PatientServiceImpl implements PatientService {

    private final PatientDAOImpl patientDAO;

    public PatientServiceImpl(PatientDAOImpl patientDAO) {
        this.patientDAO = patientDAO;
    }
    
    

    public void insertPatientRecord(Patient patient) {

    }

    public Patient listPatientById(int patientId) {
        return patientDAO.fetch(patientId);
    }

    public List<Patient> listAllPatients() {
        return patientDAO.listAll();
    }
}
