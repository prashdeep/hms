package com.edureka.hms.service;

import com.edureka.hms.model.Appointment;

import java.util.List;

/**
 * Created by prade on 4/27/2018.
 */
public interface AppointmentService {

    void bookAppointment(Appointment appointment);
    List<Appointment> listAppointmentsByDoctor(int doctorId);
    int prescribeMedicine(int appointmentId, String prescription);
}
