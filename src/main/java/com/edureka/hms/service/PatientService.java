package com.edureka.hms.service;

import com.edureka.hms.model.Patient;

import java.util.List;

/**
 * Created by prade on 5/22/2018.
 */
public interface PatientService {
    public void insertPatientRecord(Patient patient);
    public Patient listPatientById(int patientId);
    public List<Patient> listAllPatients();
}
