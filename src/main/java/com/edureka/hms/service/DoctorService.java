package com.edureka.hms.service;

import com.edureka.hms.model.Appointment;
import com.edureka.hms.model.Doctor;

import java.util.List;

/**
 * Created by prade on 5/22/2018.
 */
public interface DoctorService {

    List<Doctor> listAll();

    Doctor fetch(int doctorId);

}
