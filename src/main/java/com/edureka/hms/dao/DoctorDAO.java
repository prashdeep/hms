package com.edureka.hms.dao;

import com.edureka.hms.model.Appointment;
import com.edureka.hms.model.Doctor;
import com.edureka.hms.model.Patient;

import java.util.List;

/**
 * Created by prade on 4/27/2018.
 */
public interface DoctorDAO extends GenericDAO<Doctor> {

}
