package com.edureka.hms.dao;

import com.edureka.hms.model.Patient;
import com.edureka.hms.util.JDBCUtil;
import com.edureka.hms.util.PatientMapper;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by prade on 5/22/2018.
 */
public class PatientDAOImpl implements PatientDAO {

    private static final String FETCH_PATIENT_BY_ID = "select * from Patient where id=?";


    @Override
    public List<Patient> listAll() {
        return JDBCUtil.fetchAllPatients();
    }

    @Override
    public Patient fetch(int id) {
        try {
            PreparedStatement preparedStatement = JDBCUtil.getConnection().prepareStatement(FETCH_PATIENT_BY_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet =  preparedStatement.executeQuery();
            while (resultSet.next()){
                Patient patient = PatientMapper.mapRow(resultSet);
                return  patient;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
