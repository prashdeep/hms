package com.edureka.hms.dao;

import com.edureka.hms.model.Appointment;

import java.util.List;

/**
 * Created by prade on 5/22/2018.
 */
public interface AppointmentDAO {

    public void bookAppointment(Appointment appointment);

    public List<Appointment> listAppointmentsByDoctorId(int doctorId);

    int prescribeMedicine(int appointmentId, String prescription);
}
