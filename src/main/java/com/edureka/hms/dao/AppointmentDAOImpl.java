package com.edureka.hms.dao;

import com.edureka.hms.model.Appointment;
import com.edureka.hms.util.AppointmentMapper;
import com.edureka.hms.util.JDBCUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by prade on 5/22/2018.
 */
public class AppointmentDAOImpl implements AppointmentDAO {

    private static final String BOOK_APPOINTMENT ="INSERT INTO APPOINTMENT (APPOINTMENT_DATE, CONSULTATION_FEE, DOCTOR_ID, PATIENT_ID) VALUES (?,?,?,?)";
    private static final String LIST_BY_DOCTOR = "SELECT * FROM APPOINTMENT WHERE DOCTOR_ID=?";
    private static final String PRESCRIBE_MEDICINE = "INSERT INTO PRESCRIPTION(APPOINTMENT_ID, NOTES) VALUES (?,?)";

    @Override
    public void bookAppointment(Appointment appointment) {
        try {
            PreparedStatement preparedStatement = JDBCUtil.getConnection().prepareStatement(BOOK_APPOINTMENT);
            preparedStatement.setDate(1, new java.sql.Date(appointment.getAppointmentDate().getTime()));
            preparedStatement.setDouble(2, appointment.getConsultationFee());
            preparedStatement.setInt(3, appointment.getDoctorId());
            preparedStatement.setInt(4, appointment.getPatientId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Appointment> listAppointmentsByDoctorId(int doctorId) {
        List<Appointment> appointmentList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = JDBCUtil.getConnection().prepareStatement(LIST_BY_DOCTOR);
            preparedStatement.setInt(1, doctorId);
            ResultSet resultSet =  preparedStatement.executeQuery();
            while (resultSet.next()){
                Appointment appointment = AppointmentMapper.mapRow(resultSet);
                appointmentList.add(appointment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return appointmentList;
    }

    @Override
    public int prescribeMedicine(int appointmentId, String prescription) {
        try {
            PreparedStatement preparedStatement = JDBCUtil.getConnection().prepareStatement(PRESCRIBE_MEDICINE);
            preparedStatement.setInt(1, appointmentId);
            preparedStatement.setString(2, prescription);
            int prescriptionId =  preparedStatement.executeUpdate();
            return prescriptionId;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }
}
