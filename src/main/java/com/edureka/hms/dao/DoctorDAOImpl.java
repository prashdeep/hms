package com.edureka.hms.dao;

import com.edureka.hms.model.Appointment;
import com.edureka.hms.model.Doctor;
import com.edureka.hms.model.Patient;
import com.edureka.hms.util.DoctorMapper;
import com.edureka.hms.util.JDBCUtil;
import com.edureka.hms.util.PatientMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by prade on 5/22/2018.
 */
public class DoctorDAOImpl implements DoctorDAO {
    private static final String FETCH_DOCTOR_BY_ID = "select * from doctor where id=?";

    public List<Doctor> listAll() {
        return JDBCUtil.fetchAllDoctors();
    }


    @Override
    public Doctor fetch(int id) {
        try {
            PreparedStatement preparedStatement = JDBCUtil.getConnection().prepareStatement(FETCH_DOCTOR_BY_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet =  preparedStatement.executeQuery();
            while (resultSet.next()){
                Doctor doctor = DoctorMapper.mapRow(resultSet);
                return doctor;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
