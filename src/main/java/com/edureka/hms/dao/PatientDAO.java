package com.edureka.hms.dao;

import com.edureka.hms.model.Patient;

import java.util.List;

/**
 * Created by prade on 5/22/2018.
 */
public interface PatientDAO extends GenericDAO<Patient> {

}
