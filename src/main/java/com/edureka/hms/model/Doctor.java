package com.edureka.hms.model;

import java.util.List;

/**
 * Created by prade on 5/22/2018.
 */
public class Doctor {

    private int id;
    private String name;
    private Specialization specialization;
    private int experience;
    private List<Appointment> appointmentList;


    public Doctor(int id, String name, Specialization specialization, int experience){
        this.name = name;
        this.id = id;
        this.experience = experience;
        this.specialization = specialization;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public List<Appointment> getAppointmentList() {
        return appointmentList;
    }

    public void setAppointmentList(List<Appointment> appointmentList) {
        this.appointmentList = appointmentList;
    }
}
