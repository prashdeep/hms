package com.edureka.hms.model;

/**
 * Created by prade on 5/22/2018.
 */
public enum Specialization {

    ORTHO,
    PADIETRIC,
    DENTAL,
    GYNAC,
    KNEE_SURGEON,
    HIP_SURGEON,
    PULMOLOGIST
}
