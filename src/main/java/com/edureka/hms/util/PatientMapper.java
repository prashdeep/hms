package com.edureka.hms.util;

import com.edureka.hms.model.Patient;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by prade on 4/27/2018.
 */
public class PatientMapper {

    public static Patient mapRow(ResultSet resultSet){
        try {
            int patientID = resultSet.getInt(1);
            String patientName = resultSet.getString(2);
            Patient patient = new Patient(patientID,patientName);
            return patient;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


}
