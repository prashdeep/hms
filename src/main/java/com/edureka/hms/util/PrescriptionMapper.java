package com.edureka.hms.util;

import com.edureka.hms.model.Doctor;
import com.edureka.hms.model.Specialization;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PrescriptionMapper {

    public static Prescription mapRow(ResultSet resultSet){
        try {
            int id = resultSet.getInt(1);
            int appointmentId = resultSet.getInt(2);
            String prescription = resultSet.getString(3);
            Prescription obj = new Prescription(id, appointmentId, prescription);
            return obj;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
