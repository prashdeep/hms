package com.edureka.hms.util;

import com.edureka.hms.dao.AppointmentDAOImpl;
import com.edureka.hms.dao.DoctorDAOImpl;
import com.edureka.hms.dao.PatientDAOImpl;
import com.edureka.hms.model.Appointment;
import com.edureka.hms.model.Doctor;
import com.edureka.hms.model.Patient;
import com.edureka.hms.service.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;


/**
 * Created by prade on 4/27/2018.
 */
public class CliClient {
    /**
     * The main entry point.
     * @param ar the list of arguments
     *
     */
    private static Scanner option = new Scanner(System.in, "UTF-8");
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
    private static final double CONSULTATION_FEE = 200.00;

    private static final DoctorService doctorService = new DoctorServiceImpl(new DoctorDAOImpl());
    private static final AppointmentService appointmentService = new AppointmentServiceImpl(new AppointmentDAOImpl());
    private static final PatientService patientService = new PatientServiceImpl(new PatientDAOImpl());
    public static void main(final String[] ar) {

        System.out.println("Welcome to Health Manangement System");
        System.out.println("-----------------------");
        System.out.println("1. List All Patients");
        System.out.println("2. Display Patient Info");
        System.out.println("3. List All Doctors");
        System.out.println("4. Book Doctor's appointment");
        System.out.println("5. Doctor: View all Patient's appointments");
        System.out.println("6. Doctor: Prescribe Medicines");
        System.out.println("7. Exit");
        System.out.println("Enter your choice:");
        int menuOption = option.nextInt();
        mainMenuDetails(menuOption);
    }

    private static void mainMenuDetails(final int selectedOption) {
        switch (selectedOption) {
            case 1:
                listAllPatients();
                break;
            case 2:
                fetchPatientDetails();
                break;
            case 3:
                listAllDoctors();
            case 4:
                bookDoctorAppointment();
                break;
            case 5:
                viewAllPatientAppointments();
                break;
            case 6:
                prescribeMedicine();
                break;
              case 7:
                Runtime.getRuntime().halt(0);
            default:
                System.out.println("Choose valid options");
        }
    }

    private static void listAllDoctors() {
        List<Doctor> doctorList = doctorService.listAll();
        Iterator<Doctor> iterator = doctorList.iterator();
        while (iterator.hasNext()) {
            Doctor doctor = iterator.next();
            printDoctorDetails(doctor);
        }
    }

    private static void prescribeMedicine() {
        System.out.println("Please enter appointment Id : ");
        int appointmentId = option.nextInt();
        System.out.println("Please enter the prescription");
        String prescription = option.next();
        int prescriptionId = appointmentService.prescribeMedicine(appointmentId, prescription);
        System.out.printf("Prescription Updated in records with ID: "+prescriptionId);
    }

    private static void viewAllPatientAppointments() {
        System.out.println("Please enter Doctor's ID");
        int doctorId  = option.nextInt();
        List<Appointment> appointmentList = appointmentService.listAppointmentsByDoctor(doctorId);
        Iterator<Appointment> iterator = appointmentList.iterator();
        while (iterator.hasNext()){
            Appointment appointment = iterator.next();
            System.out.println("-----------------------------------------------------");
            System.out.println("Patient's Name = "+patientService.listPatientById(appointment.getPatientId()).getName());
            System.out.println("Doctor' Name = "+doctorService.fetch(doctorId).getName());
            System.out.println("Date of Appointment = " + dateFormat.format(appointment.getAppointmentDate()));
            System.out.println("-----------------------------------------------------");
        }
    }

    private static void bookDoctorAppointment() {
        System.out.println("Please enter Patient Id");
        int patientId = option.nextInt();
        System.out.println("Please enter Doctor's Id");
        int doctorId = option.nextInt();
        System.out.println("Please enter the appointment Date in dd/mm/yy format");
        String strDate = option.next();
        Date appointmentDate = null;
        try {
            appointmentDate = dateFormat.parse(strDate);
            if (appointmentDate.compareTo(new Date()) < 0){
                appointmentDate = new Date();
            }
        } catch (ParseException e) {
            System.out.println("Error parsing Date: Setting the appointment to current Date");
            appointmentDate = new Date();
            e.printStackTrace();
        }
        Appointment appointment = new Appointment(appointmentDate, doctorId, patientId, CONSULTATION_FEE);
        appointmentService.bookAppointment(appointment);
    }

    private static void fetchPatientDetails() {
        System.out.println("Please enter your OutPatient ID");
        int patientID = option.nextInt();
        Patient patient = patientService.listPatientById(patientID);
        printPatientDetails(patient);
    }

    private static void listAllPatients() {
        List<Patient> patientList = patientService.listAllPatients();
        Iterator<Patient> iterator = patientList.iterator();
        while (iterator.hasNext()) {
            Patient patient = iterator.next();
            printPatientDetails(patient);
        }
    }

    private static void printPatientDetails(Patient patient){
        System.out.println("-------------------------");
        System.out.println("Patient ID: "+patient.getId());
        System.out.println("Patient Name: "+patient.getName());
        System.out.println("Patient DOB:"+patient.getDob());
    }

    private static void printDoctorDetails(Doctor doctor){
        System.out.println("-------------------------");
        System.out.println("Doctor ID: "+doctor.getId());
        System.out.println("Doctor Name: "+doctor.getName());
        System.out.println("Doctor Specialization: "+doctor.getSpecialization().toString());
        System.out.println("Doctor Experience:"+ doctor.getExperience());
    }

}
