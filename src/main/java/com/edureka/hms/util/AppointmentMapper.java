package com.edureka.hms.util;

import com.edureka.hms.model.Appointment;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class AppointmentMapper {
    public static Appointment mapRow(ResultSet resultSet){
        try {
            Date date = resultSet.getDate(2);
            double consultationFee = resultSet.getDouble(3);
            int doctorId = resultSet.getInt(4);
            int patientId = resultSet.getInt(5);
            Appointment appointment = new Appointment(date, doctorId,patientId, consultationFee);
            return appointment;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
