package com.edureka.hms.util;

import com.edureka.hms.model.Doctor;
import com.edureka.hms.model.Specialization;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DoctorMapper  {
    public static Doctor mapRow(ResultSet resultSet){
        try {
            int doctorId = resultSet.getInt(1);
            String name = resultSet.getString(2);
            Specialization specialization = Specialization.valueOf(resultSet.getString(3));
            int experience = resultSet.getInt(4);
            Doctor doctor = new Doctor(doctorId, name, specialization, experience);
            return doctor;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}

