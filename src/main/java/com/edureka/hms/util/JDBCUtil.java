package com.edureka.hms.util;

import com.edureka.hms.model.Doctor;
import com.edureka.hms.model.Patient;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by prade on 4/27/2018.
 */
public class JDBCUtil {

    public static final String DATABASE_NAME = "hms";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "welcome";

    private static final String FETCH_ALL_PATIENTS = "select * from Patient";
    private static final String FETCH_ALL_DOCTORS = "select * from DOCTOR";

    public static Connection getConnection(){
        Connection connection = null;
        try{
            connection = DriverManager.getConnection("jdbc:mysql://localhost/"+DATABASE_NAME, USERNAME, PASSWORD);
        } catch (SQLException e) {
            System.out.println("Exception connection to the Database :: " +e.getMessage());
            e.printStackTrace();
        }
        return connection;
    }

    public boolean execute(String sql) {
        try(Statement statement = getStatement();){
            return statement.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private static Statement getStatement(){
        Connection connection = getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return statement;
    }


    public static final List<Patient> fetchAllPatients(){
        ResultSet resultSet = null;
        List<Patient> patientsList = new ArrayList<>();
        try (Statement statement = getStatement()) {
            resultSet = statement.executeQuery(FETCH_ALL_PATIENTS);
            while (resultSet.next()){
                Patient patient = new PatientMapper().mapRow(resultSet);
                patientsList.add(patient);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return patientsList;
    }

    public static final List<Doctor> fetchAllDoctors(){
        ResultSet resultSet = null;
        List<Doctor> doctorList = new ArrayList<>();
        try (Statement statement = getStatement()) {
            resultSet = statement.executeQuery(FETCH_ALL_DOCTORS);
            while (resultSet.next()){
                Doctor doctor = new DoctorMapper().mapRow(resultSet);
                doctorList.add(doctor);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return doctorList;
    }
}
