package com.edureka.hms.util;

public class Prescription {

    private int id;
    private String text;
    private int appointmentId;

    public Prescription(int id, int appointmentId, String text) {
        this.id = id;
        this.text = text;
        this.appointmentId = appointmentId;
    }

    public int getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(int appointmentId) {
        this.appointmentId = appointmentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
